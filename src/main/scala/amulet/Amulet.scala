package amulet

import javafx.application.Application
import javafx.event.ActionEvent
import javafx.scene.Scene
import javafx.scene.control.Button
import javafx.scene.layout.StackPane
import javafx.stage.Stage

object Amulet
{
  def main(args: Array[String])
  {
    Application.launch(classOf[Amulet], args: _*)
  }
}

class Amulet extends Application {
  override def start(primaryStage: Stage)
  {
    primaryStage.setTitle("Amulet")
    val button = new Button
    button.setText("A Shiny Button")
    button.setOnAction((e: ActionEvent) => {
      println("Hello Amulet!")
    })

    val root = new StackPane
    root.getChildren.add(button)
    primaryStage.setScene(new Scene(root, 800, 600))
    primaryStage.show
  }
}
